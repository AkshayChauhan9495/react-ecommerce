import React, { Component } from "react";

export default class LiftingMen extends Component {
  constructor(props) {
    super(props);
  }

  addItem = () => {
    this.props.addData(this.props.products);
  };

  render() {
    return (
      <div className="d-flex justify-content-center mb-5">
        <button onClick={this.addItem}>Add New Item</button>
      </div>
    );
  }
}
