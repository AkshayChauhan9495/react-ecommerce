import React from "react";
import { div } from "react-router-dom";
import { Link } from "react-router-dom";
import Login from "./Login";
import "./Navbar.css";
// import Adddata from "./Adddata";
import Search from "./Search";
class Navbar extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    console.log(this.props.outputData);
    return (
      <div>
        <nav
          className="navbar navbar-expand-lg  navbar-dark py-3"
          style={{ background: "whitesmoke" }}
        >
          <div className="container">
            <Link exact to="./" className="navbar-brand text-secondary">
              Retail Therapy
            </Link>

            <button
              className="navbar-toggler bg-secondary"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navmenu"
            >
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse p-3" id="navmenu">
              <ul className="navbar-nav ms-auto align-items-center">
                <li className="nav-item ">
                  <Link exact to="/" className="nav-link text-secondary px-4">
                    Home
                  </Link>
                </li>
                <li className="nav-item">
                  <Link
                    exact
                    to="/men"
                    className="nav-link text-secondary px-4"
                  >
                    Mens's Clothing
                  </Link>
                </li>
                <li className="nav-item">
                  <Link
                    exact
                    to="/women"
                    className="nav-link text-secondary px-4"
                  >
                    Women's Clothing
                  </Link>
                </li>
                <li className="nav-item">
                  <Link
                    exact
                    to="/gadgets"
                    className="nav-link text-secondary px-4"
                  >
                    Gadgets
                  </Link>
                </li>
                <li className="nav-item">
                  <Link exact to="/jewellery" className="nav-link px-4">
                    Jewellery
                  </Link>
                </li>
                <li className="nav-item"></li>
              </ul>
              {/* <Login /> */}
            </div>
            <Search
              placeholder="search dresses"
              setData={this.props.outputData}
              filtered={this.props.filters}
            />
          </div>
          <div></div>
        </nav>
      </div>
    );
  }
}

// <div className="navbar">
//   <div class="hamburger">
//     <div></div>
//     <div></div>
//     <div></div>
//   </div>
//   <div className="name">
//     <h2>Retail Therapy</h2>
//   </div>
//   <div class="cart-icon">
//     <i class="fa fa-shopping-cart"></i>
//   </div>
//   <div className="link-div">
//     <Link exact to="/" className="nav-link">
//       Home
//     </Link>
//     <Link exact to="/men" className="nav-link">
//       Mens's Clothing
//     </Link>
//     <Link exact to="/women" className="nav-link">
//       Women's Clothing
//     </Link>
//     <Link exact to="/gadgets" className="nav-link">
//       Electronics
//     </Link>
//     <Link exact to="/jewellery" className="nav-link">
//       Jewellery
//     </Link>
//     {/* <Link to className="nav-link">Mens's Clothing</>
//           <div className="nav-link">Women's Clothing</div>
//           <div className="nav-link">Electronics</div>
//           <div className="nav-link">Jewellery</div> */}
//   </div>
// </div>;

export default Navbar;
