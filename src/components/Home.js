import React, { Component } from "react";
import mens from "./images/mens-clothing.jpg";
import women from "./images/women-clothing.jpg";
import gadgets from "./images/gadgets.jpg";
import jewelery from "./images/jewelery.jpg";
import "./Home.css";
import { Link } from "react-router-dom";
export default class Home extends Component {
  render() {
    return (
      <div>
        <div>
          <div className="card">
            <img src={mens} className="men img" />
            <p>Men's Clothing</p>
            <div className="button-1">
              <Link to="/men">
                <button>Shop Now</button>
              </Link>
            </div>
          </div>
          <div className="card">
            <img src={women} className="women img" />
            <p>Women's Clothing</p>
            <div className="button-1">
              <Link to="/women">
                <button>Shop Now</button>
              </Link>
            </div>
          </div>
          <div className="section-3">
            <div className="card">
              <img src={gadgets} className="men gadget-img" />
              <p>Gadgets</p>
              <div className="button-1">
                <Link to="/gadgets">
                  <button>Shop Now</button>
                </Link>
              </div>
            </div>
            <div className="card">
              <img src={jewelery} className="jewelery img" />
              <p>Jewellery</p>
              <div className="button-1">
                <Link to="/jewellery">
                  <button>Shop Now</button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
