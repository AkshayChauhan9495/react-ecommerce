import React from "react";
import { Link } from "react-router-dom";
import "./Footer.css";
import Login from "./Login";

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <footer>
          <div className="content">
            <div className="left box">
              <div className="upper">
                <div className="topic">About us</div>
                <p>
                  Becoming India’s no. 1 fashion destination is not an easy
                  feat. Sincere efforts, digital enhancements and a team of
                  dedicated personnel with an equally loyal customer base have
                  made Retail Therapy the online platform that it is today. The
                  original B2B venture for personalized gifts was conceived in
                  2007 but transitioned into a full-fledged ecommerce giant
                  within a span of just a few years. By 2012, Retail Therapy had
                  introduced 350 Indian and international brands to its
                  platform, and this has only grown in number each passing year.
                </p>
              </div>
              <div className="lower">
                <div className="topic">Contact us</div>
                <div className="phone">
                  <a href="#">+91 956036****</a>
                </div>
                <div className="email">
                  <a href="#">abc@gmail.com</a>
                </div>
              </div>
            </div>
            <div className="middle box">
              <div className="topic">Our Work</div>
              <div>
                <a href="#">Smart men’s clothing </a>
              </div>
              <div>
                <a href="#">Trendy women’s clothing</a>
              </div>
              <div>
                <a href="#">Stylish accessories</a>
              </div>
            </div>
            <div className="right box">
              <div className="topic">Subscribe us</div>
            </div>
          </div>
          <div className="bottom">
            <p>
              Copyright © 2021 <a href="#">Retail Therapy</a> All rights
              reserved
            </p>
          </div>
        </footer>
      </>
    );
  }
}

export default Footer;
