import React, { Component } from "react";
import Loader from "./Loader";
import Error from "./Error";
import NoProducts from "./NoProducts";
import Footer from "./Footer";
// import "./Men.css";
import "./style.css";
import LiftingMen from "./LiftingMen";
import jewellery from "./images/jewel.jpg";
import Updation from "./Updation";
import Delete from "./Delete";
import Update from "./Update";
export default class Jewellery extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   output: [],
    //   isLoader: true,
    //   isError: false,
    //   notFoundtheData: false,
    // };
    this.state = {
      result: this.props.data3,
      delData: this.props.data,
    };
  }
  addItem = (products) => {
    fetch("https://fakestoreapi.com/products", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: "test product",
        price: "13.5",
        description: "lorem ipsum set",
        image: "https://i.pravatar.cc",
        category: "electronic",
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        let allData = products;
        allData.push(data);
        this.setState({ result: allData });
      });
  };

  updateProduct = (id, products) => {
    fetch("https://fakestoreapi.com/products/" + id, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: "test product",
        price: 13.5,
        description: "lorem ipsum set",
        image: "https://i.pravatar.cc",
        category: "electronic",
      }),
    })
      .then((res) => res.json())
      .then((json) => {
        console.log(id);
        products[id] = json;
        this.setState({
          result: products,
        });
      });
  };
  delData = (index) => {
    const tempArr = this.state.result;
    // console.log(tempArr.filter((ele) => ele.id !== index));
    this.setState({ result: tempArr.filter((ele) => ele.id !== index) });
    console.log(this.state.result);
  };

  componentDidMount() {
    this.updateProduct();
  }

  updItem = (changeData, index) => {
    console.log(changeData);
    let res = this.state.result;
    console.log(res[index]);
    const obj = {
      title: changeData.title,
      price: changeData.price,
      image: res[index].image,
    };

    res[index] = obj;

    this.setState({
      productData: res,
    });
  };

  // componentDidMount = async () => {
  //   const addr = window.location.pathname;
  //   console.log(addr);
  //   const res = await fetch(
  //     `https://fakestoreapi.com/products/category/jewelery`
  //   );
  //   console.log(res, "res");
  //   this.setState({
  //     notFoundtheData: true,
  //     isLoader: false,
  //     isError: true,
  //   });
  //   const data = await res.json();
  //   this.setState({
  //     output: data,
  //     isLoader: false,
  //     isError: false,
  //   });
  //   console.log(data);
  // };
  render() {
    // const { data3 } = this.props;
    const { result } = this.state;

    // console.log(this.state.output);
    return (
      <div>
        {/* {this.state.isLoader === true ? (
          <Loader />
        ) : ( */}
        <div>
          <img src={jewellery} className="men img" />
          <h1 className="title text-center pt-5 ">Jewellery</h1>
          <div className="card-1 m-5">
            {result.map((key, index) => (
              <div
                key={key.id}
                className="card-2 d-flex flex-column flex-md-row p-2  align-items-center justify-content-between"
              >
                <div className="image">
                  <img src={key.image} className="img-1 p-5" />
                </div>
                <div className="details d-flex-col align-self-center p-5 ">
                  <h1 className="heading">{key.title}</h1>
                  <p className="fs-5 description text-justify">
                    <strong>Description: </strong> {key.description}
                  </p>
                  <p className="bold fs-5 description text-justify">
                    <strong>Price: </strong>${key.price}
                  </p>
                  {/* <p className="bold fs-5 description text-justify">
                    <strong>Ratings: </strong> {key.rating.rate}
                  </p>
                  <p className="bold fs-5 description text-justify">
                    <strong>Counts: </strong>
                    {key.rating.count}
                  </p> */}
                </div>
                <Update data={key} index={index} changeData={this.updItem} />
                <Delete del={this.delData} data={result} id={key.id} />
              </div>
            ))}
          </div>
        </div>

        <LiftingMen addData={this.addItem} products={this.props.data3} />
      </div>
    );
  }
}
