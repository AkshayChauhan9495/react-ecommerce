import React, { Component } from "react";

export default class SearchData extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    // console.log(this.props.data4);
    return (
      <div>
        {this.props.data4.map((key) => {
          return (
            <div
              key={key.id}
              className="card-2 d-flex flex-column flex-md-row p-2 align-items-center border border-light mb-1"
            >
              <div className="image">
                <img src={key.image} className="img-1 p-5" />
              </div>
              <div className="details d-flex-col align-self-center p-5 ">
                <h1 className="heading">{key.title}</h1>
                <p className="fs-5 description text-justify">
                  <strong>Description: </strong> {key.description}
                </p>
                <p className="bold fs-5 description text-justify">
                  <strong>Price: </strong>${key.price}
                </p>
                <p className="bold fs-5 description text-justify">
                  <strong>Ratings: </strong> {key.rating.rate}
                </p>
                <p className="bold fs-5 description text-justify">
                  <strong>Counts: </strong>
                  {key.rating.count}
                </p>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}
