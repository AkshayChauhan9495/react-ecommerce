// import { Button } from "bootstrap";
import { Button } from "bootstrap";
import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
    };
  }

  clickFn = (e) => {
    // e.preventDefault();
    console.log(this.state);
    this.props.filtered(this.state.search);
  };

  changingValue = (e) => {
    let val = e.target.value;
    this.setState({
      search: val,
    });
  };

  render() {
    console.log(this.state);
    return (
      <div>
        <nav class="navbar navbar-light bg-light">
          <form class="form-inline">
            <input
              class="form-control mr-sm-2"
              type="search"
              placeholder={this.props.placeholder}
              onChange={this.changingValue}
              name="search"
              aria-label="Search"
            />
            <Link to="/searched">
              <button
                class="btn btn-outline-success my-2 my-sm-0"
                type="submit"
                onClick={this.clickFn}
              >
                Search
              </button>
            </Link>
          </form>
        </nav>
      </div>
    );
  }
}
