import React, { Component } from "react";

export default class Delete extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state = {
      data: this.props.data,
    };
  }

  deleteItem = () => {
    this.props.del(this.props.id);
  };
  render() {
    return (
      <>
        <button onClick={this.deleteItem}>Delete Item</button>
      </>
    );
  }
}
