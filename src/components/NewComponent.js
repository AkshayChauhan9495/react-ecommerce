import React, { Component } from "react";

export default class NewComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      obj: [],
    };
  }

  componentDidMount = () => {
    fetch("https://fakestoreapi.com/products", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: "test product",
        price: "13.5",
        description: "lorem ipsum set",
        image: "https://i.pravatar.cc",
        category: "electronic",
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState({ obj: data });
      });
  };

  render() {
    const { obj } = this.state;
    console.log(obj);
    return (
      <div className="card-2 d-flex flex-column flex-md-row p-2 align-items-center">
        <img src={obj.image} className="img-1 p-5" />
        <div className="details d-flex-col align-self-center p-5 ">
          <h1 className="heading">{obj.title}</h1>
          <p className="fs-5 description">
            <strong>Description: </strong> {obj.description}
          </p>
          <p className="bold fs-5 description">
            <strong>Price: </strong>${obj.price}
          </p>
        </div>
      </div>
    );
  }
}
