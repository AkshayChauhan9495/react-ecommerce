import React, { Component } from "react";
// import Loader from "./Loader";
// import Error from "./Error";
// import NoProducts from "./NoProducts";
// import Footer from "./Footer";
import { Link } from "react-router-dom";
// import "./Men.css";
import "./style.css";
import mens from "./images/mens-clothing.jpg";
import LiftingMen from "./LiftingMen";
import Updation from "./Updation";
import Delete from "./Delete";
export default class Men extends Component {
  constructor(props) {
    super(props);
    // console.log(this.props.data);
    this.state = {
      result: this.props.data,
      delData: this.props.data,
    };
  }

  addItem = (products) => {
    fetch("https://fakestoreapi.com/products", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: "test product",
        price: "13.5",
        description: "lorem ipsum set",
        image: "https://i.pravatar.cc",
        category: "electronic",
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        let allData = products;
        allData.push(data);
        this.setState({ result: allData });
      });
  };

  // componentDidMount() {
  //   this.addItem();
  // }

  delData = (index) => {
    const tempArr = this.state.result;
    // console.log(tempArr.filter((ele) => ele.id !== index));
    this.setState({ result: tempArr.filter((ele) => ele.id !== index) });
    console.log(this.state.result);
  };

  updateProduct = (id, products) => {
    fetch("https://fakestoreapi.com/products/" + id, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: "test product",
        price: 13.5,
        description: "lorem ipsum set",
        image: "https://i.pravatar.cc",
        category: "electronic",
      }),
    })
      .then((res) => res.json())
      .then((json) => {
        console.log(id);
        products[id] = json;
        this.setState({
          result: products,
        });
      });
  };

  componentDidMount() {
    this.updateProduct();
  }

  render() {
    const { result } = this.state;

    return (
      <div>
        <div>
          <div>
            <img src={mens} className="men img " />
            <h1 className="title text-center pt-5 ">Men's Clothing</h1>
            <div className="card-1 m-5">
              {result.map((key, index) => (
                <div
                  key={key.id}
                  className="card-2 d-flex flex-column flex-md-row p-2 justify-content-between align-items-center border border-light mb-1"
                >
                  <div className="image">
                    <Link to={"/products/" + key.id}>
                      <img src={key.image} className="img-1 p-5" />
                    </Link>
                  </div>
                  <div className="details d-flex-col align-self-center p-5 ">
                    <h1 className="heading">{key.title}</h1>
                    <p className="fs-5 description text-justify">
                      <strong>Description: </strong> {key.description}
                    </p>
                    <p className="bold fs-5 description text-justify">
                      <strong>Price: </strong>${key.price}
                    </p>
                    <p className="bold fs-5 description text-justify">
                      {/* <strong>Ratings: </strong> {key.rating.rate} */}
                    </p>
                    {/* <p className="bold fs-5 description text-justify">
                      <strong>Counts: </strong>
                      {key.rating.count}
                    </p> */}
                  </div>
                  <Updation
                    updItem={this.updateProduct}
                    data={result}
                    id={index}
                  />
                  <Delete del={this.delData} data={result} id={key.id} />
                </div>
              ))}

              <LiftingMen addData={this.addItem} products={this.props.data} />
            </div>
          </div>

          {/* {this.state.result.map((key) => (
            <div>
              <p>{key.title}</p>
              <img src={key.image} />
            </div>
          ))}
          <LiftingMen addData={this.addItem} products={this.state.result} /> */}
        </div>
      </div>
    );
  }
}
