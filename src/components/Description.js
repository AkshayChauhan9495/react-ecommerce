import React, { Component } from "react";

export default class Description extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arr: [],
      error: false,
    };
  }
  componentDidMount() {
    const path = window.location.pathname;
    const pathArray = path.split("/");
    const id = pathArray[pathArray.length - 1];
    fetch("https://fakestoreapi.com/products/" + id)
      .then((res) => res.json())
      .then((data) => {
        console.log("Data:" + data);
        this.setState({ arr: data, error: true });
      });
  }
  render() {
    return (
      <div>
        {this.state.error ? <div>{this.state.arr.title}</div> : undefined}
      </div>
    );
  }
}
