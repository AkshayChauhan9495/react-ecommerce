import React, { Component } from "react";

export default class Updation extends Component {
  constructor(props) {
    super(props);
    // console.log(this.props);
  }

  update = () => {
    this.props.updItem(this.props.id, this.props.data);
  };

  render() {
    return (
      <div>
        <button onClick={this.update}>Update Item</button>
      </div>
    );
  }
}
