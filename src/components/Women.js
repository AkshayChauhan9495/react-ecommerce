import React, { Component } from "react";
// import Loader from "./Loader";
// import Error from "./Error";
// import NoProducts from "./NoProducts";
// import Footer from "./Footer";
// import "./Men.css";
import Delete from "./Delete";
import "./style.css";
import { Link } from "react-router-dom";
import LiftingMen from "./LiftingMen";
import women from "./images/women-clothing.jpg";
import Updation from "./Updation";
export default class Women extends Component {
  constructor(props) {
    // super(props);
    // this.state = {
    //   output: [],
    //   isLoader: true,
    //   isError: false,
    //   notFoundtheData: false,
    // };
    super(props);
    this.state = {
      result: this.props.data1,
      delData: this.props.data,
    };
  }

  addItem = (products) => {
    fetch("https://fakestoreapi.com/products", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: "test product",
        price: "13.5",
        description: "lorem ipsum set",
        image: "https://i.pravatar.cc",
        category: "electronic",
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        let allData = products;
        allData.push(data);
        this.setState({ result: allData });
      });
  };
  delData = (index) => {
    const tempArr = this.state.result;
    // console.log(tempArr.filter((ele) => ele.id !== index));
    this.setState({ result: tempArr.filter((ele) => ele.id !== index) });
    console.log(this.state.result);
  };

  updateProduct = (id, products) => {
    fetch("https://fakestoreapi.com/products/" + id, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        title: "test product",
        price: 13.5,
        description: "lorem ipsum set",
        image: "https://i.pravatar.cc",
        category: "electronic",
      }),
    })
      .then((res) => res.json())
      .then((json) => {
        console.log(id);
        products[id] = json;
        this.setState({
          result: products,
        });
      });
  };

  // componentDidMount = async () => {
  //   // const addr = window.location.pathname;
  //   // console.log(addr);
  //   const res = await fetch(
  //     `https://fakestoreapi.com/products/category/women's clothing`
  //   );
  //   this.setState({
  //     notFoundtheData: true,
  //     isLoader: false,
  //     isError: true,
  //   });
  //   const data = await res.json();
  //   this.setState({
  //     output: data,
  //     isLoader: false,
  //     isError: false,
  //   });
  //   console.log(data);
  // };

  componentDidMount() {
    this.updateProduct();
  }

  render() {
    // const { data1 } = this.props;
    const { result } = this.state;

    // console.log(result, "data result");
    return (
      <div>
        {/* {this.state.isLoader === true ? (
          <Loader />
        ) : ( */}
        <div>
          <div>
            <img src={women} className="men img" />
            <h1 className="title text-center pt-5 ">Women's Clothing</h1>
            <div className="card-1 m-5">
              {result.map((key, index) => (
                <div
                  key={key.id}
                  className="card-2 d-flex flex-column flex-md-row p-2  align-items-center justify-content-between"
                >
                  <div className="image">
                    <Link to={"/products/" + key.id}>
                      <img src={key.image} className="img-1 p-5" />
                    </Link>
                  </div>
                  <div className="details d-flex-col align-self-center p-5 ">
                    <h1 className="heading">{key.title}</h1>
                    <p className="fs-5 description text-justify">
                      <strong>Description: </strong> {key.description}
                    </p>
                    <p className="bold fs-5 description text-justify">
                      <strong>Price: </strong>${key.price}
                    </p>
                  </div>
                  <Updation
                    updItem={this.updateProduct}
                    data={result}
                    id={index}
                  />
                  <Delete del={this.delData} data={result} id={key.id} />
                </div>
              ))}
              <LiftingMen addData={this.addItem} products={this.props.data1} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
