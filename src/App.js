import React, { Component } from "react";
import Footer from "./components/Footer";
import Navbar from "./components/Navbar";
// import Products from "./components/Products";
import Home from "./components/Home";
import Men from "./components/Men";
import Women from "./components/Women";
import Gadgets from "./components/Gadgets";
import Jewellery from "./components/Jewellery";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Description from "./components/Description";
import SearchData from "./components/SearchData";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      output: [],
      mens: [],
      womens: [],
      accessories: [],
      gadgets: [],
      output1: [],
    };
  }

  setData = (data) => {
    const men = data.filter((key) => {
      return key.category === "men's clothing";
    });
    const women = data.filter((key) => {
      return key.category === "women's clothing";
    });
    const accessory = data.filter((key) => {
      return key.category === "jewelery";
    });
    const gadget = data.filter((key) => {
      return key.category === "electronics";
    });
    // const titles = data.map((key) => {
    //   return key.title;
    // });
    this.setState({
      output: data,
      mens: men,
      womens: women,
      accessories: accessory,
      gadgets: gadget,
      // output1: titles,
    });
  };

  filteredData = (value) => {
    const { output } = this.state;
    const res = output.filter((key) => {
      if (key.title.toLowerCase().includes(value.toLowerCase())) {
        return true;
      }
    });
    this.setState({
      output1: res,
    });
  };

  fetchAllData = async () => {
    const res = await fetch("https://fakestoreapi.com/products");
    const data = await res.json();
    this.setData(data);
  };

  componentDidMount = () => {
    this.fetchAllData();
  };

  render() {
    const { output, mens, womens, accessories, gadgets, output1 } = this.state;
    // console.log({ output1 });
    return (
      <BrowserRouter>
        <div>
          <Navbar outputData={output1} filters={this.filteredData} />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/men" element={<Men data={mens} />} />
            <Route path="/women" element={<Women data1={womens} />} />
            <Route path="/gadgets" element={<Gadgets data2={gadgets} />} />
            <Route
              path="/jewellery"
              element={<Jewellery data3={accessories} />}
            />
            <Route path="/searched" element={<SearchData data4={output1} />} />
            <Route path="/products/:id" element={<Description />} />
          </Routes>

          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}
